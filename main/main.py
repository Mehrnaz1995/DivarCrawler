from bs4 import BeautifulSoup
import requests
import json


def findURL(class_name, URL):
    mainPage = requests.get(URL)
    mainP = BeautifulSoup(mainPage.text, 'html.parser')
    items_list_div = mainP.find(class_=class_name)
    list_items = items_list_div.find_all('a')
    link_list = []
    for itemCategory in list_items:
        link_list.append('https://divar.ir' + itemCategory.get('href'))
    return link_list


def findAddTags(add):
    tag_list = []
    spans = add.findAll('span')
    for span in spans:
        tag_list.append(span.get_text())
    return tag_list


def find10Adds(categoryURL):
    mainPage = requests.get(categoryURL)
    mainP = BeautifulSoup(mainPage.text, 'html.parser')
    addsTagsClassList = mainP.find_all(class_='ui card post-card')
    return list(addsTagsClassList[:10])


def findAddItems (content):
    items = content.findAll(class_='item')
    item_dict={}
    for item in items:
        item_dict[str(item.find('label').get_text())] = str(item.find(class_='value').get_text())
    return item_dict


def get_add_content(add_URL):
    mainPage = requests.get(add_URL)
    mainP = BeautifulSoup(mainPage.text, 'html.parser')
    add_Content_ClassList = mainP.find_all(class_ ='content')
    return add_Content_ClassList[3]


def get_category_name(class_name,page_URL):#TODO
    category_name_list=[]
    mainPage = requests.get(page_URL)
    mainP = BeautifulSoup(mainPage.text, 'html.parser')
    itemsListDiv = mainP.find(class_=class_name)
    for item in itemsListDiv:
        print(item)
        category_name_list_tag=item.findAll('h3')
        print(len(category_name_list))
        for tag in category_name_list_tag:
            category_name_list.append(tag.find(class_='normal-heading').get_text())
            print(tag.find(class_='normal-heading').get_text())


def write_to_file(dict,name):
    json_dict = json.dumps(dict, ensure_ascii=False, indent=4, sort_keys=True)
    with open('%s.json' % name, 'w') as outfile:
        outfile.write(json_dict)


# category list links
mainPageLinkList = findURL('ui pointing vertical menu', 'https://divar.ir/tehran/%D8%AA%D9%87%D8%B1%D8%A7%D9%86/browse/')

i = 0
for category in mainPageLinkList:
    print(category)
    category_dict = {}
    tenAdds=find10Adds(category)
    j=0
    for add in tenAdds:

        add_tag_list = findAddTags(add)
        addURL = findURL('ui card post-card', category)

        content = get_add_content(addURL[0])
        add_dict=findAddItems(content)
        if add_tag_list.__contains__("چت"):
            add_dict["chat"]=True
        else:
            add_dict["chat"] = False
        if add_tag_list.__contains__("قوری"):
            add_dict["urgent"]=True
        else:
            add_dict["urgent"] = False
        j += 1
        category_dict[j]=add_dict
        write_to_file(add_dict,str(j)+"add")
    i += 1
    write_to_file(category_dict, str(i))
    print("done with the "+str(i)+"th category")



